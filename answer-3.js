class World {
  constructor(num) {
    // membuat atribut cities yang isinya berupa array dari object City
    // yang berjumlah sesuai dengan value num
    this.name = name;
    this.num = num;
  }

  add_city(name) {
    // add city by name
    this.name = name;
  }

  random_city() {
    // mengembalikan random city dari entitas cities
    return this.name;
  }

  total_cities() {
    // mengembalikan jumlah cities yang ada di world
    return this.num;
  }
}

class City {
  constructor(name) {
    // jika pembuatan object City disertai name maka city akan dibuat dengan
    // name tersebut. Jika tidak, generate nama kota dengan
    // 5 huruf random misal "abcde"
    // buat atribut citizens dengan nilai awal dari method add_citizen
  }

  add_citizen() {
    // tambah citizen ke atribut citizens
  }
}

class Citizen {
  constructor() {
    // buat atribut age dengan value berupa umur yang merupakan nilai random
    // dari 0 sampai 100
    this.age = Math.floor(Math.random() * 101);
  }
}

const world = new World(50);
world.add_city("Jakarta");
console.log("Random city name: ", world.random_city().name);
// harusnya mengeluarkan value semacam "Jakarta" atau nilai random lainnya yang panjang characternya 5.
console.log("Age of first citizen in another random city: ", world.random_city().citizens[0]);
// harusnya mengeluarkan sebuah angka dari 0-100
console.log("# of Cities: ", world.total_cities());
// harusnya mengeluarkan angka 51
