const values = {
  name: "John Doe",
  age: 21,
  username: "johndoe",
  password: "abc123",
  location: "Jakarta",
  time: new Date(),
};

const valuesStr = JSON.stringify(values, (key, value) => {
  if (key === "password" || key === "time") {
    return undefined;
  }
  return value;
});

console.log(valuesStr);
